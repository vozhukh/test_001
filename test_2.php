<?php /**
		 * @author v.ozhukh
		 * @copyright 2015
		 */
		class FilesList {
		    private $dir = '' ;
		    public function __construct( $dir = "" ) {
		        $this->dir = $dir ;
		    }
		    function get() {
		        $arResult = array() ;
		      
		        $nameTpmplate = '/^[a-zA-Z0-9]+\.txt$/' ;
		        if ( is_dir( $this->dir ) ) {
		            // открываем каталог
		            if ( $dh = opendir( $this->dir ) ) {
		                while ( ( $file = readdir( $dh ) ) !== false ) {
		                    if ( $file == "." || $file == '..' ) continue ;
		                    if ( preg_match( $nameTpmplate, $file ) ) {
		                        $arResult[] = $file ;

		                    }
		                }
		                // закрываем каталог
		                closedir( $dh ) ;
		            }
		             natsort( $arResult ) ;
                    return $arResult;
		        }
		        else {
		            echo $this->dir . ' -такой директории нет;<br>' ;
		        }
		    }


		}
		$fl = new FilesList( './files' ) ;
		$res = $fl->get() ;
		if ( is_array( $res ) ) {
		    foreach ( $res as $list ) {
		        echo "$list \n" ;
		    }
		} ?>