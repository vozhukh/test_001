<?php /**
		 * @author v.ozhukh
		 * @copyright 2015
		 */
		final class Init {

		    public function __construct( $dbhost = "", $dbuser = "", $dbpass = "", $dbname = '' ) {
		        $conn = mysql_connect( $dbhost, $dbuser, $dbpass ) ;
		        if ( ! $conn ) {
		            die( 'Could not connect: ' . mysql_error() ) ;
		        }
		        $conn = mysql_select_db( $dbname, $conn ) ;
		        if ( ! $conn ) {
		            die( 'Can\'t use ' . $dbname . ' : ' . mysql_error() ) ;
		        }

		        $this->create() ; 
		        $this->fill() ;
		    }
		    public function __destruct() {
		        mysql_close() ;
		    }

		    protected function create() {
		        $dump = "
                    CREATE TABLE IF NOT EXISTS test(
                    id INT NOT NULL AUTO_INCREMENT, 
                    PRIMARY KEY(id),
                     script_name VARCHAR(255), 
                     start_time INT,
                     end_time   INT,
                     Result ENUM ('normal', 'illegal', 'failed', 'success')
                     );
                     DELETE FROM test
                " ;
		        $sql = explode( ';', $dump ) ;
		        foreach ( $sql as $key => $val ) {
		            mysql_query( $val ) or die( mysql_error() ) ;
		        }


		    }
		    protected function fill() {
		        $arRes = array(
		            'normal',
		            'illegal',
		            'failed',
		            'success' ) ;
		        for ( $i = 1; $i <= 5; $i++ ) {
		            $arInsert[] = "('" . str_shuffle( md5( microtime() ) ) . "', '" . rand( 100, 1000 ) . "', '" . rand( 1000, 100000 ) . "', '" . $arRes[rand( 0,
		                count( $arRes ) - 1 )] . "' )" ;
		        }
		        $arInsert = implode( ", ", $arInsert ) ;
		        mysql_query( "INSERT INTO `test`( `script_name`, `start_time`, `end_time`, `Result`) VALUES $arInsert " ) or die( mysql_error() ) ;
		    }

		    public function get() {
		        $query = "SELECT * FROM test WHERE Result IN ('normal', 'success')" ;
		        $result = mysql_query( $query ) or die( 'Error: ' . mysql_error() ) ;

		        echo "<table>\n" ;
		        while ( $line = mysql_fetch_array( $result, MYSQL_ASSOC ) ) {
		            echo "\t<tr>\n" ;
		            foreach ( $line as $col_value ) {
		                echo "\t\t<td>$col_value</td>\n" ;
		            }
		            echo "\t</tr>\n" ;
		        }
		        echo "</table>\n" ;

		        mysql_free_result( $result ) ;
		    }
		}
		$newInit = new Init( 'localhost', 'b15', 'b15', 'b15' ) ;

		$newInit->get() ; ?>